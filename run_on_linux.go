package main

import (
	"os/exec"
	"fmt"
	"strings"
	"os"
)


func run_on_linux() {

	// Get connected usb devices io file location
	fmt.Println("Getting usb device information ...\n")
	
	lsusb_output_bytes, err := exec.Command("lsusb").Output()
	lsusb_output := string(lsusb_output_bytes)
	check_err(err)
	
	fmt.Println(lsusb_output + "\n")
	

	// Locate phone device
	lsusb_output_slice := strings.Split(lsusb_output, "\n")

	// check args
	if len(os.Args) != 2 {
		fmt.Println("Error!\nWants device name as an argument.")
		os.Exit(1)
	}

	var phone_usb_info string
	for ord := range lsusb_output_slice {
		if strings.Contains(lsusb_output_slice[ord], os.Args[1]) == true {
			
			fmt.Println("Found.")
			fmt.Println(lsusb_output_slice[ord] + "\n")
			phone_usb_info = lsusb_output_slice[ord]
		}
	}


	phone_bus_num := phone_usb_info[4:7]
	phone_dev_num := phone_usb_info[15:18]
	phone_io_file_path := "/dev/bus/usb/" + phone_bus_num + "/" + phone_dev_num


	// Using sudo chmod ... to open access of the device
	chmod_cmd := exec.Command("sudo", "chmod", "666", phone_io_file_path)
	
	fmt.Println("Changing premission of phone device io file ..." + "\n")
	chmod_cmd.Start()
	chmod_cmd.Wait()

	// Stop adb server
	stop_adb_cmd := exec.Command("adb", "kill-server")
	fmt.Println("Stopping adb server ...")
	stop_adb_cmd.Start()
	stop_adb_cmd.Wait()
	fmt.Println("Stopped.\n")
	
	// start adb
	fmt.Println("Starting adb daemon ...")
	start_adb_cmd := exec.Command("adb", "start-server")
	start_adb_out, _ := start_adb_cmd.CombinedOutput()
	fmt.Println(string(start_adb_out) + "\n")
	
	fmt.Println(`which adb command do you want to be executed ?
1. Activate brevent
2. Just help me set up premission and adb
`)

	// Read input
	var i int
	_, err = fmt.Scanf("%d", &i)
	if err != nil {
		fmt.Println("error: excepted integer")
		
		fmt.Println("Stopping adb server ...\n")
		stop_adb_cmd.Start()
		stop_adb_cmd.Wait()
		os.Exit(1)
	}

	if i == 1 {

		adb_activate_brevent_cmd_str := "adb -d shell sh /data/data/me.piebridge.brevent/brevent.sh"
		adb_activate_brevent_cmd_slice := strings.Split(adb_activate_brevent_cmd_str, " ")
		adb_activate_brevent_cmd := exec.Command(adb_activate_brevent_cmd_slice[0], adb_activate_brevent_cmd_slice[1:]...)
		adb_activate_brevent_out, _ := adb_activate_brevent_cmd.CombinedOutput()
		fmt.Println(string(adb_activate_brevent_out) + "\n")
		fmt.Println("Done.\n")
		
	} else if i == 2 {
		return
	}
}
