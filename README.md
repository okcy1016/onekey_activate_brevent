# onekey_activate_brevent
一键激活黒域。

目前仅支持 Linux。

## 前提条件
已安装 adb 工具并保证其处于 path 中。

## 编译
```
git clone https://github.com/okcy1016/onekey_activate_brevent
cd onekey_activate_brevent
go build -ldflags="-s -w" -o onekey_activate_brevent
```
